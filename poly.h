#ifndef RS_POLY_H
#define RS_POLY_H

#include <stdint.h>

void rs_poly_scale(uint8_t *x, int xlen, uint8_t k);
void rs_poly_mul(uint8_t *z, const uint8_t *x, int xlen, const uint8_t *y, int ylen);
void rs_poly_add(uint8_t *x, int xlen, const uint8_t *y, int ylen);
void rs_poly_mod(uint8_t *x, int xlen, const uint8_t *d, int dlen);
uint8_t rs_poly_eval(const uint8_t *p, int plen, uint8_t x);
void rs_poly_formal_derivative(uint8_t *d, const uint8_t *x, int xlen);
#endif
