#include "gf.h"
#include "poly.h"
#include <assert.h>

// Scale the polynomial x by k (in place).
void rs_poly_scale(uint8_t *x, int xlen, uint8_t k) {
    int i;
    for (i = 0; i < xlen; i++)
        x[i] = rs_gf_mul(x[i], k);
}

// Multiply the polynomial x by polynomial y.
// The output length is xlen + ylen (as the multiplication is carryless).
void rs_poly_mul(uint8_t *z, const uint8_t *x, int xlen, const uint8_t *y, int ylen) {
    int i, j;

    // z = 0
    for (i = 0; i < xlen + ylen; i++)
        z[i] = 0;

    for (j = 0; j < ylen; j++) {
        // z += x * y_j
        for (i = 0; i < xlen; i++)
            z[j + i] = rs_gf_add(z[j + i], rs_gf_mul(x[i], y[j]));
    }
}

// Sum two polynomials. x is modified in-place. xlen must be >= ylen.
void rs_poly_add(uint8_t *x, int xlen, const uint8_t *y, int ylen) {
    int i;

    assert(xlen >= ylen);

    for (i = 0; i < xlen; i++)
        x[i] = (i < ylen) ? rs_gf_add(x[i], y[i]) : x[i];
}

// Remainder of polynomial division (performed in place).
// * dlen is assumed to be smaller or equal to xlen.
// * d is assumed not to have leading zeros.
void rs_poly_mod(uint8_t *x, int xlen, const uint8_t *d, int dlen) {
    int i, j;
    uint8_t n;

    assert(d[dlen - 1] != 0);

    for (j = xlen - dlen; j >= 0; j--) {
        // Divide the leading coefficient of x by that of d.
        n = rs_gf_div(x[j + dlen - 1], d[dlen - 1]);

        // x -= d * n
        for (i = dlen - 1; i >= 0; i--)
            x[j + i] = rs_gf_sub(x[j + i], rs_gf_mul(d[i], n));
    }
}

// Evaluates a polynomial using the Horner method.
uint8_t rs_poly_eval(const uint8_t *p, int plen, uint8_t x) {
    int i;
    uint8_t z;

    z = 0;
    for (i = plen - 1; i >= 0; i--) {
        z = rs_gf_mul(z, x);
        z = rs_gf_add(z, p[i]);
    }

    return z;
}

// Formal derivative of x, d is xlen - 1 long.
void rs_poly_formal_derivative(uint8_t *d, const uint8_t *x, int xlen) {
    int i;

    // Formal derivatives involve multiplication outside of the field,
    //  but in GF(2^8), x times a yields either x (when a is odd) or
    //  zero (when a is even).
    for (i = 1; i < xlen; i++)
        d[i - 1] = (i & 1) ? x[i] : 0;
}
