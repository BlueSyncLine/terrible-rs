#ifndef RS_GF_H
#define RS_GF_H
#include <stdint.h>
#include "tab/rstab.h"

#define rs_gf_add(x, y) ((x) ^ (y))
#define rs_gf_sub(x, y) rs_gf_add(x, y)
#define rs_gf_inv(x) (RS_GF_TAB_INV[x])
#define rs_gf_exp2(x) (RS_GF_TAB_EXP2[x])
#define rs_gf_log2(x) (RS_GF_TAB_LOG2[x])

// Note that the addition here is outside of the field.
#define rs_gf_mul(x, y) (((x) == 0 || (y) == 0) ? 0 : rs_gf_exp2(rs_gf_log2(x) + rs_gf_log2(y)))

#define rs_gf_div(x, y) (rs_gf_mul(x, rs_gf_inv(y)))
#endif
