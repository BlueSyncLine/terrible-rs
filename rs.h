#ifndef RS_H
#define RS_H
#include <stdint.h>

// Total codeword length.
#define RS_M 255

// Data-bearing symbols.
#define RS_N 239

// Check symbols.
#define RS_2T (RS_M - RS_N)
#define RS_T (RS_2T / 2)

void rs_encode(uint8_t *codeword);
void rs_syndromes(const uint8_t *codeword, uint8_t *syndromes);
void rs_berlekamp_massey(const uint8_t *codeword, uint8_t *locator, uint8_t *syndromes);
int rs_forney_correct(uint8_t *codeword, uint8_t *locator, uint8_t *syndromes);
int rs_correct(uint8_t *codeword);
#endif
