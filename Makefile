CFLAGS = -g -Wall -Wextra

all: clean libterriblers.a tests

clean:
	rm -f *.o libterriblers.a tests

rstab.o:
	gcc -c  $(CFLAGS) tab/rstab.c -o rstab.o

rs.o:
	gcc -c $(CFLAGS) rs.c -o rs.o

poly.o:
	gcc -c $(CFLAGS) poly.c -o poly.o

libterriblers.a: rs.o rstab.o poly.o
	ar -rcs libterriblers.a rs.o rstab.o poly.o

tests.o:
	gcc -c $(CFLAGS) -Wno-unused -o tests.o tests.c

tests: tests.o libterriblers.a
	gcc $(CFLAGS) -Wno-unused -o tests tests.o -L. -lterriblers
	#./tests
