#ifndef RS_TAB_H
#define RS_TAB_H
#include <stdint.h>

extern const uint8_t RS_GF_TAB_INV[];
extern const uint8_t RS_GF_TAB_EXP2[];
extern const uint8_t RS_GF_TAB_LOG2[];
extern const uint8_t RG_GF_TAB_XINV[];
#endif
