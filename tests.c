#include <stdlib.h>
#include <stdio.h>
#include "rs.h"
#include <assert.h>

/*void print_hex(uint8_t *b, int n) {
    int i;
    for (i = 0; i < n; i++)
        printf("%02x", b[i]);
    printf("\n");
}*/

int main(int argc, char *argv[]) {
    int i, numErrors;
    uint8_t codeword[RS_M], syndromes[RS_2T];

    for (numErrors = 0; numErrors <= 9; numErrors++) {
        for (i = 0; i < RS_N; i++)
            codeword[i] = i;

        rs_encode(codeword);
        rs_syndromes(codeword, syndromes);
        for (i = 0; i < RS_2T; i++)
            assert(syndromes[i] == 0);

        // The message should have no errors at this point.
        assert(rs_correct(codeword) == 0);

        // Corrupt the message.
        for (i = 0; i < numErrors; i++)
            codeword[123 + i * 5] ^= i + 5;

        if (numErrors != 9) {
            // Make sure the errors are detected correctly.
            assert(rs_correct(codeword) == numErrors);

            // Verify.
            for (i = 0; i < RS_N; i++)
                assert(codeword[i] == i);
        } else {
            // Over 8 errors are uncorrectable.
            assert(rs_correct(codeword) == -1);
        }
    }

    return EXIT_SUCCESS;
}
