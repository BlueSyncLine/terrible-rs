#include "rs.h"
#include "poly.h"
#include "gf.h"
#include "tab/rstab.h"

#define RS_POLY_G_LEN 17
const uint8_t RS_POLY_G[RS_POLY_G_LEN] = {
    0x3b, 0x24, 0x32, 0x62, 0xe5, 0x29, 0x41, 0xa3, 0x08, 0x1e, 0xd1, 0x44, 0xbd, 0x68, 0x0d, 0x3b,
    0x01
};

// a = 0x02
#define rs_a_i(i) RS_GF_TAB_EXP2[i]

// Encodes a codeword in-place. The bytes corresponding to the check symbols
//  are overwritten.
void rs_encode(uint8_t *codeword) {
    int i;
    uint8_t p[RS_M];

    // Copy the codeword into the dividend (multiplied by x^2t).
    for (i = 0; i < RS_M; i++)
        p[i] = (i >= RS_2T) ? codeword[i - RS_2T] : 0;

    // Compute the polynomial modulo.
    // TODO this can be optimized (the largest coefficient of G is 1, so no division is necessary for scaling!)
    rs_poly_mod(p, RS_M, RS_POLY_G, RS_POLY_G_LEN);

    // Copy the remainder where it belongs.
    for (i = 0; i < RS_2T; i++)
        codeword[RS_N + i] = p[i];
}

// Computes the syndromes for a given codeword.
void rs_syndromes(const uint8_t *codeword, uint8_t *syndromes) {
    int i;

    for (i = 0; i < RS_2T; i++)
        syndromes[i] = rs_poly_eval(codeword, RS_M, rs_a_i(i));
}

// Ref: https://en.wikipedia.org/wiki/Berlekamp%E2%80%93Massey_algorithm
void rs_berlekamp_massey(const uint8_t *codeword, uint8_t *locator, uint8_t *syndromes) {
    int i, l, m, n, reset;
    uint8_t locator_prev[RS_T + 1], locator_swap[RS_T + 1], z[RS_T + 1], b, d;

    l = 0;
    b = 1;
    m = 1;

    // Initialize the polynomials.
    for (i = 0; i < RS_T + 1; i++) {
        locator[i] = (i == 0) ? 1 : 0;
        locator_prev[i] = locator[i];
    }

    // Compute the syndromes.
    rs_syndromes(codeword, syndromes);

    for (n = 0; n < RS_2T; n++) {
        // Compute the discrepancy.
        d = syndromes[n];
        for (i = 1; i < l + 1; i++)
            d = rs_gf_add(d, rs_gf_mul(syndromes[n - i], locator[i]));

        if (d == 0) {
            m++;
        } else {
            reset = (2 * l <= n);

            if (reset) {
                // locator -> locator_swap
                for (i = 0; i < RS_T + 1; i++)
                    locator_swap[i] = locator[i];
            }

            // z = locator_prev * x^m
            for (i = 0; i < RS_T + 1; i++)
                z[i] = (i - m >= 0) ? locator_prev[i - m] : 0;

            // z *= d / b
            rs_poly_scale(z, RS_T + 1, rs_gf_div(d, b));

            // locator += z
            rs_poly_add(locator, RS_T + 1, z, RS_T + 1);

            if (reset) {
                // locator_swap -> locator_prev
                for (i = 0; i < RS_T + 1; i++)
                    locator_prev[i] = locator_swap[i];

                b = d;
                m = 1;
                l = n + 1 - l;
            } else {
                m++;
            }
        }
    }
}

// Apply the Forney algorithm and correct the errors.
int rs_forney_correct(uint8_t *codeword, uint8_t *locator, uint8_t *syndromes) {
    int i, errors;
    uint8_t u[RS_T + 1 + RS_2T], v[RS_T + 1], e, t;

    // u = locator * syndromes
    rs_poly_mul(u, locator, RS_T + 1, syndromes, RS_2T);

    // v = formalDerivative(locator) * x
    v[0] = 0;
    rs_poly_formal_derivative(v + 1, locator, RS_T + 1);

    errors = 0;
    // e_j = - u(Xj^-1) / v(Xj^-1)
    for (i = 0; i < RS_M; i++) {
        t = rs_gf_inv(rs_a_i(i));

        // Find and correct errors (denoted by roots of the error locator polynomial).
        if (rs_poly_eval(locator, RS_T + 1, t) == 0) {
            // Note that u is truncated to 2T here.
            e = rs_gf_div(rs_poly_eval(u, RS_2T, t), rs_poly_eval(v, RS_T + 1, t));
            codeword[i] = rs_gf_add(codeword[i], e);
            errors++;
        }
    }

    // If, after correcting the error, any of the syndromes are still non-zero, we failed.
    rs_syndromes(codeword, syndromes);
    for (i = 0; i < RS_2T; i++) {
        if (syndromes[i] != 0)
            return -1;
    }

    // Return the number of errors corrected otherwise.
    return errors;
}

// Try correting the codeword.
int rs_correct(uint8_t *codeword) {
    uint8_t locator[RS_T + 1], syndromes[RS_2T];
    rs_berlekamp_massey(codeword, locator, syndromes);
    return rs_forney_correct(codeword, locator, syndromes);
}
